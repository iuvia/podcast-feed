FROM python:3.8-alpine

WORKDIR /app
ADD bottle.py /app/bottle.py
ADD main.py /app/main.py

ADD feed.rss.xml /app/feed.rss.xml

EXPOSE 8080

ENTRYPOINT ["python3", "/app/main.py"]
