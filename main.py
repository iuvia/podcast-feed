#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from bottle import route, run, static_file

# Future:
# ROOT_CONTENT_DIR = os.environ.get('ROOT_CONTENT_DIR', '/var/lib/podcast')
ROOT_CONTENT_DIR = os.environ.get('ROOT_CONTENT_DIR', os.path.dirname(__file__))

@route('/feed.rss.xml')
def index():
    return static_file('feed.rss.xml', root=ROOT_CONTENT_DIR, mimetype='application/rss+xml')


if __name__ == '__main__':
    # run(host='0.0.0.0', port=8081, debug=True, reloader=True)
    run(host='0.0.0.0', port=8080)

